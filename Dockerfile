FROM openjdk:8-jre-alpine

LABEL name="jrbm/static-code-analysis" \
	  version="0.1.0" \
	  maintainer="JR Barreras <barreras@unl.edu>"


WORKDIR /opt

RUN apk add --no-cache curl sed bash su-exec
RUN mkdir -p /opt/src 

RUN curl --insecure -o ./sonarscanner.zip -L https://sonarsource.bintray.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.1.0.1141.zip 
RUN unzip sonarscanner.zip 
RUN rm sonarscanner.zip

ENV SONAR_RUNNER_HOME=/opt/sonar-scanner-3.1.0.1141
ENV PATH $PATH:/opt/sonar-scanner-3.1.0.1141/bin

COPY entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/entrypoint.sh

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD /bin/bash
