[![build status](https://git.unl.edu/barreras/static-code-analysis/badges/master/build.svg)](https://git.unl.edu/barreras/static-code-analysis/commits/master)

Docker image for static code analysis
test CI

Build
sudo docker build -t jrbm/static-code-analysis .


Test
sudo docker run -ti jrbm/static-code-analysis


Use
`
sudo docker run -ti -v $(pwd):/opt/src:Z -w/opt/src/wireless-reg/ jrbm/static-code-analysis ls 
sudo docker run -ti -v $(pwd):/opt/src:Z -e LOCAL_USER_ID=$(id -u $USER) -w/opt/src/wireless-reg/ jrbm/static-code-analysis sonar-scanner -Dsonar.projectKey=wireless-reg -Dsonar.sources=. -Dsonar.host.url=http://10.160.238.11:9000 -Dsonar.login=df9ebaa9d91268c879c4cff41ff70652ac8766db -Dsonar.java.binaries=.
`
...